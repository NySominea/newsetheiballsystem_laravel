<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallSystemAccountLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_system_account_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('account_id')->index();
            $table->unsignedInteger('to_account_id')->default(0)->index();
            $table->unsignedInteger('manager_id')->default(0)->index();
            
            $table->double('amount')->default(0.00);
            $table->double('balance')->default(0.00);
            $table->string('abstract');
            $table->string('user_abstract')->nullable();
            $table->string('log_number',40)->nullable();
            $table->unsignedTinyInteger('log_type')->default(1);
            $table->unsignedTinyInteger('to_type')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_system_account_logs');
    }
}
