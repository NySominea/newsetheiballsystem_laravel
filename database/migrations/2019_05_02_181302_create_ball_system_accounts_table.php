<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallSystemAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_system_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',100);
            $table->string('number',60);
            $table->string('name',100);
            $table->double('balance')->default(0.00);
            $table->boolean('state')->default(1);
            $table->unsignedInteger('sort')->default(0);

            $table->unsignedInteger('type_id')->index();
            $table->unsignedInteger('category_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_system_accounts');
    }
}
