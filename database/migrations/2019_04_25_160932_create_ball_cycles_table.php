<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallCyclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_cycles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cycle_sn',100);
            $table->boolean('state')->default(TRUE);
            $table->boolean('has_released')->default(FALSE);
            $table->string('stopped_time',100);
            $table->string('result_time',100);
            $table->text('result_number')->nullable();
            $table->boolean('is_jackpot')->default(FALSE);
            $table->double('prize')->default(0);
            $table->double('prize_5d')->default(0);
            $table->double('prize_4d')->default(0);
            $table->double('prize_3d')->default(0);
            $table->double('lucky_draw')->default(0);

            $table->unsignedTinyInteger('game_type_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_cycles');
    }
}
