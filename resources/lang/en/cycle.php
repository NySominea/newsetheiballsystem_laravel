<?php

return [
    'no_cycle' => 'The cycle has stopped!',
    'no_order' => 'There is no order found!',
    'out_of_range_bet' => 'The bet number is out of range!',
    'no_ticket' => 'There is no ticket found!',
    'no_config' => 'There is game config found!',
    'cycle_stop' => 'The current cycle has stopped!',
    'failed_to_order' => 'Your orders are failed to save! Please try again!'
];