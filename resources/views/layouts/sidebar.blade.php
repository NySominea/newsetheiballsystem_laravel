<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Sethei Ball</div> <i class="icon-menu" title="Main"></i></li>

<li class="nav-item">
    <a href="{{route('dashboard')}}" class="nav-link"><i class="icon-home4"></i> <span>Dashboard</span></a>
</li>

@canany(['view-user-account','add-new-user-account','user-account-modification','user-take-cash','view-user-account-log','user-recharge'])
<li class="nav-item nav-item-submenu 
    {{ in_array(Route::currentRouteName(), ['users.sale-network.create','users.sale-network.edit','users.account.recharge.edit',
    'users.account.takecash.edit','users.accounts.show','users.logs.show']) 
        ? ' nav-item-expanded nav-item-open' : ''}}">
    <a href="#" class="nav-link"><i class="icon-users4"></i> <span>{{ $settings['language']['LANG_MENU_USER_LIST'] }}</span></a>
    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        <li class="nav-item">
            <a href="{{route('users.sale-network.index')}}" class="nav-link
                {{ in_array(Route::currentRouteName(),['users.sale-network.create','users.sale-network.edit','users.account.recharge.edit',
                    'users.account.takecash.edit','users.accounts.show','users.logs.show']) ? ' active' : '' }}">
                {{ $settings['language']['LANG_MENU_SALE_NET'] }}
            </a>
        </li>
        <li class="nav-item"><a href="{{route('users.direct-user.index')}}" class="nav-link">{{ $settings['language']['LANG_MENU_DIRECT_USER'] }}</a></li>
    </ul>
</li>
@endcanany

@canany(['view-order-list','export-order-list'])
<li class="nav-item">
    <a href="{{route('orders.temp.index')}}" class="nav-link">
        <i class="icon-stack4"></i><span>{{ $settings['language']['LANG_MENU_THIS_ORDER'] }}</span>
    </a>
</li>
@endcanany

@canany(['view-history-report','export-history-report'])
<li class="nav-item">
    <a href="{{route('orders.report.selection-form')}}" class="nav-link" data-active="{{ in_array(Route::currentRouteName(), ['orders.report.index']) ? true : false}}">
        <i class="icon-stack4"></i><span>{{ $settings['language']['LANG_MENU_REPORT'] }}</span>
    </a>
</li>
@endcanany

@canany(['view-transaction-log','view-platform-account','platform-account-access-transfer'])
<li class="nav-item nav-item-submenu
    {{ in_array(Route::currentRouteName(), ['accounts.categories.index','accounts.categories.create','accounts.categories.edit','accounts.create','accounts.edit',
        'accounts.deposit.edit','accounts.transfer.edit']) 
    ? ' nav-item-expanded nav-item-open' : ''}}">
    <a href="#" class="nav-link"><i class="icon-coins"></i> <span>{{ $settings['language']['LANG_MENU_FINANCE'] }}</span></a>
    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        @canany(['view-platform-account','platform-account-access-transfer'])
        <li class="nav-item">
            <a href="{{route('accounts.index')}}" class="nav-link 
                {{ in_array(Route::currentRouteName(), ['accounts.categories.index','accounts.categories.create',
                'accounts.categories.edit','accounts.create','accounts.edit','accounts.deposit.edit',
                'accounts.transfer.edit']) ? ' active' : ''}}">
                {{ $settings['language']['LANG_MENU_ACCOUNT'] }}
            </a>
        </li>
        @endcanany
        @can('view-transaction-log')
        <li class="nav-item">
            <a href="{{route('accounts.logs.index')}}" class="nav-link 
            {{ in_array(Route::currentRouteName(), ['accounts.logs.index']) ? ' active' : ''}}">
                {{ $settings['language']['LANG_MENU_ACCOUNT_LOG'] }}
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcanany

@canany(['lottery-setting','lottery-operation','billing-operation','view-lottery-log'])
<li class="nav-item nav-item-submenu 
    {{ in_array(Route::currentRouteName(), ['cycles.result.set','cycles.result.settle']) 
        ? ' nav-item-expanded nav-item-open' : ''}}">
    <a href="#" class="nav-link"><i class="icon-alarm"></i> <span>{{ $settings['language']['LANG_MENU_CYCLE'] }}</span></a>

    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        @canany(['lottery-setting','lottery-operation','billing-operation'])
        <li class="nav-item"><a href="{{ route('cycles.set.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['cycles.result.set','cycles.result.settle']) ? ' active' : ''}}">{{ $settings['language']['LANG_MENU_CYCLE_SET']}}</a></li>
        @endcanany
        @canany(['view-lottery-log'])
        <li class="nav-item"><a href="{{ route('cycles.logs.index') }}" class="nav-link">{{ $settings['language']['LANG_MENU_CYCLE_LOG']}}</a></li>
        @endcanany
    </ul>
</li>
@endcanany

@canany(['lucky-draw'])
<li class="nav-item nav-item-submenu 
    {{ in_array(Route::currentRouteName(), ['profit.lucky-draw.index']) 
        ? ' nav-item-expanded nav-item-open' : ''}}">
    <a href="#" class="nav-link"><i class="icon-cash"></i> <span>Profit Sharing</span></a>

    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        @canany(['lucky-draw'])
        <li class="nav-item"><a href="{{ route('profit.lucky-draw.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['profit.lucky-draw.index']) ? ' active' : ''}}">Monthly Lucky Draw</a></li>
        @endcanany
    </ul>
</li>
@endcanany

@canany(['view-manager-account','add-new-manager-account','manager-account-modification','view-role','add-new-role','role-modification','view-manager-log'])
<li class="nav-item nav-item-submenu 
    {{ in_array(Route::currentRouteName(), ['managers.create','managers.edit','managers.roles.create','managers.roles.edit']) 
        ? ' nav-item-expanded nav-item-open' : ''}}">
    <a href="#" class="nav-link"><i class="icon-user-check"></i> <span>{{ $settings['language']['LANG_MENU_MANAGER']}}</span></a>

    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        @canany(['view-manager-account','add-new-manager-account','manager-account-modification'])
        <li class="nav-item"><a href="{{ route('managers.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['managers.create','managers.edit']) ? ' active' : ''}}">{{ $settings['language']['LANG_MENU_MANAGER_LIST']}}</a></li>
        @endcanany
        @canany(['add-new-role','view-role','role-modification'])
        <li class="nav-item"><a href="{{ route('managers.roles.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['managers.roles.create','managers.roles.edit']) ? ' active' : ''}}">{{ $settings['language']['LANG_MENU_MANAGER_ROLE']}}</a></li>
        @endcanany
        @canany(['view-manager-log'])
        <li class="nav-item"><a href="{{ route('managers.logs.index') }}" class="nav-link">{{ $settings['language']['LANG_MENU_MANAGER_LOG']}}</a></li>
        @endcanany
    </ul>
</li>
@endcanany

@canany(['view-language-account','language-set-modification','view-exchange-rate-set','exchange-rate-modification','view-location-set-modification','location-set-modification'])
<li class="nav-item nav-item-submenu 
    {{ in_array(Route::currentRouteName(), []) 
        ? ' nav-item-expanded nav-item-open' : ''}}">
    <a href="#" class="nav-link"><i class="icon-cog3"></i> <span>{{ $settings['language']['LANG_MENU_SYSTEM_SET']}}</span></a>

    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        @canany(['view-language-set','language-set-modification'])
            <li class="nav-item"><a href="{{ route('system-set.languages.admin.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['view-language-set','language-set-modification']) ? ' active' : ''}}">{{ $settings['language']['LANG_MENU_LANG_SET']}}</a></li>
        @endcanany

        @canany(['view-exchange-rate-set','exchange-rate-modification'])
            <li class="nav-item"><a href="{{ route('system-set.exchange-rate.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['view-exchange-rate-set','exchange-rate-modification']) ? ' active' : ''}}">{{ $settings['language']['LANG_MENU_EXCHANGE_SET']}}</a></li>
        @endcanany

        @canany(['view-location-set','location-set-modification'])
            <li class="nav-item"><a href="{{ route('system-set.locations.provinces.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['view-location-set','location-set-modification']) ? ' active' : ''}}">Location Set</a></li>
        @endcanany
    </ul>
</li>
@endcanany

@canany(['view-game-type-setting','game-type-modification','view-game-rebate-setting','game-rebate-modification'])
<li class="nav-item nav-item-submenu">
    <a href="#" class="nav-link"><i class="icon-cog3"></i> <span>{{ $settings['language']['LANG_MENU_GAME_SET']}}</span></a>

    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        @canany(['view-game-type-setting','game-type-modification'])
        <li class="nav-item"><a href="{{ route('games.types.index') }}" class="nav-link">Game Type</a></li>
        @endcanany
        @canany(['view-game-rebate-setting','game-rebate-modification'])
        <li class="nav-item"><a href="{{ route('games.rebates.index') }}" class="nav-link">{{ $settings['language']['LANG_MENU_GAME_REBATE']}}</a></li>
        @endcanany
        @canany(['view-game-promotion','game-promotion-modification'])
        <li class="nav-item"><a href="{{ route('games.promotion.index') }}" class="nav-link">Game Promotion</a></li>
        @endcanany
    </ul>
</li>
@endcanany
@canany(['analysis-operation'])
<li class="nav-item">
    <a href="{{route('analysis.index')}}" class="nav-link">
        <i class="icon-chart"></i><span>Analysis</span>
    </a>
</li>
@endcanany
