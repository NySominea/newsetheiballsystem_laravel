@if(session('success'))
<div class="alert bg-success text-white alert-styled-left alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    <span class="font-weight-semibold">Well done!</span> {{session('success')}}
</div>
@endif

