@extends('layouts.master')

@section('custom-css')
    <style>
        .input-group input{
            height: 65px;
            font-size: 16px;
        }
    </style>
@endsection

@section('content')
<!— Page header —>
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Result Analysis</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Analysis</span>
                <span class="breadcrumb-item active">Result</span>
            </div>
        </div>
    </div>
</div>
<!— /page header —>

<div class="content">
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header py-2">
                    {{ Form::button('<i class="icon-chart mr-2"></i> Analyze',['form' => 'analysis', 'type' => 'submit', 'class' => 'btn btn-sm btn-success d-inline float-right p-1']) }}
                </div>
                
                <div class="card-body">
                    @include('includes.error-msg')
                    @include('includes.success-msg')
                    {{ Form::open(['route' => 'analysis.index', 'method' => 'GET', 'id' => 'analysis']) }}
                    <div class="form-group row">
                        <div class="col-lg-12">  
                            <div class="input-group">
                                @for($i=1;$i<7;$i++)
                                {{ Form::number('no'.$i, isset($_GET['no'.$i]) ? $_GET['no'.$i] : old('no'.$i), ['class' => "text-center form-control result ", 'id' => 'no'.$i, 'placeholder' => 'No'.$i, 'min' => 01, 'max' => 35, 'maxlength' => 2]) }}
                                @endfor
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                    @if(isset($data) && count($data))
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                        @foreach($data as $key => $d)
                            <li class="nav-item">
                                <a class="nav-link {{$key == '6D' ? 'active' :''}}" id="n-{{$key}}-tab" data-toggle="tab" href="#n-{{$key}}">{{strtoupper($key)}}</a>
                            </li>
                        @endforeach
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            @foreach($data as $key => $d)
                                <div class="tab-pane fade {{$key == '6D' ? 'show active' :''}}" id="n-{{$key}}" role="tabpanel" aria-labelledby="n-{{$key}}-tab">
                                    <div class="row pt-1 pb-1">
                                        <div class="table-sorter-wrapper col-lg-12 table-responsive">
                                            <table class="table table-hover table-bordered">
                                                <thead class="">
                                                    <tr class="table-active">
                                                        <th> Bet Number </th>
                                                        <th> {{ $settings['language']['LANG_LABEL_AMOUNT'] }}(R) </th>
                                                        <th> {{ $settings['language']['LANG_LABEL_AMOUNT'] }}($) </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($d) > 0)
                                                    @foreach($d as $order)
                                                        <tr>
                                                            <td>{{ implode(', ',[$order->no1,$order->no2,$order->no3,$order->no4,$order->no5,$order->no6])}}</td>
                                                            <td>{{ currencyFormat($order->amount) }}</td>
                                                            <td>{{ currencyFormat($order->amount * $settings['exchangeRate']['rielToDollar']) }}</td>
                                                        </tr>
                                                    @endforeach
                                                    @else 
                                                    <tr> <td colspan="3"> No record!!! </td></tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div> 
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="bg-slate-800">
                                <th>Bet Number</th>
                                <th>{{ $settings['language']['LANG_LABEL_AMOUNT'] }} (R)</th>
                                <th>{{ $settings['language']['LANG_LABEL_AMOUNT'] }} ($)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($orders) && $orders->count() > 0)
                                @foreach($orders as $key => $row)
                                <tr>
                                    <td>{{ implode(', ',[$row->no1,$row->no2,$row->no3,$row->no4,$row->no5,$row->no6]) }}</td>
                                    <td>{{ currencyFormat($row->amount) }}</td>
                                    <td>{{ currencyFormat($row->amount * $settings['exchangeRate']['rielToDollar']) }}</td>
                                </tr>
                                @endforeach
                            @else 
                                <tr><td colspan="3">No Data</td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                @if(isset($orders))
                <div class="card-footer">
                    @if($orders->hasMorePages())
                        <div class="mb-2">
                            {!! $orders->appends(Input::except('page'))->render() !!}
                        </div>
                    @endif
                    <div>
                        Showing {{$orders->firstItem()}} to {{$orders->lastItem()}}
                        of  {{$orders->total()}} entries
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    
</div>
@endsection