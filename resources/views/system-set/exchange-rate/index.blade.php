@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_MENU_EXCHANGE_SET'] }}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_SYSTEM_SET'] }}</span>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_LABEL_EXCHANGE_RATE'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.error-msg')
    @include('includes.success-msg')
    <div class="card">
        {!! Form::open(['route' => 'system-set.exchange-rate.store','method' => 'POST']) !!}
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="bg-slate-800">
                                    <th>Currency A</th>
                                    <th>Currency B</th>
                                    <th>Exchange Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($exchangeRate) && $exchangeRate->count() > 0)
                                    <tr>
                                        <td>{{ $exchangeRate->from ? $exchangeRate->from->name . '('.$exchangeRate->from->unit.')' : '' }}</td>
                                        <td>{{ $exchangeRate->to ? $exchangeRate->to->name . '('.$exchangeRate->to->unit.')' : '' }}</td>
                                        <td>{{Form::number('rate',$exchangeRate->rate,['class' => 'w-100 form-control form-control-sm', 'step' => '0.01'])}}</td>
                                    </tr>
                                @else 
                                    <tr><td colspan="3">No Data</td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @canany(['exchange-rate-modification'])
            @if(isset($exchangeRate) && $exchangeRate->count() > 0)
            <div class="card-footer">
                {!! Form::button('<i class="icon-folder mr-1"></i> '.$settings['language']['LANG_LABEL_POST'], [ 'class' => 'btn btn-success', 'type' => 'submit']) !!}
            </div>
            @endif
        @endcanany
        {!! Form::close() !!}
    </div>
</div>
@endsection