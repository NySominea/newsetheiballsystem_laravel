@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Game Rebate</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Game</span>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_LABEL_REBATE'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.error-msg')
    @include('includes.success-msg')
    {!! Form::open(['route' => 'games.rebates.store','method' => 'POST']) !!}
    @csrf
    <div class="card">
        <div class="card-header">
            @php 
                $group = [
                    '0' => 'Default',
                    '2563' => 'Phnom Penh',
                    '2730' => 'Phnom Penh Master',
                    '2444' => 'Shop'
                ] 
            @endphp
            @foreach( $group as $id => $label)
            <h5>{{ $label }}</h5>
            <div class="table-responsive mb-3">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>Game Name</th>
                            @for($i=1;$i<9;$i++)
                                <th>{{getUserLevelTitle($i)}}</th>
                            @endfor
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($rebates) && $rebates->count() > 0)
                            @foreach($rebates[$id] as $row)
                            <tr>
                                <td>{{ $row->gameType->name }}</td>
                                @for($i=1;$i<9;$i++)
                                    <td style="min-width:60px;">{{Form::number("rebates[$row->id][l".$i."_rebate]",$row->{'l'.$i.'_rebate'},['class' => 'w-100 form-control', 'step' => '0.01'])}}</td>
                                @endfor
                            </tr>
                            @endforeach
                        @else 
                            <tr><td colspan="6">No Data</td></tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @endforeach
        </div>
        @canany(['game-rebate-modification'])
        @if(isset($rebates) && $rebates->count() > 0)
        <div class="card-footer">
            {!! Form::button('<i class="icon-folder mr-1"></i> '.$settings['language']['LANG_LABEL_POST'], [ 'class' => 'btn btn-success', 'type' => 'submit']) !!}
        </div>
        @endif
        @endcanany
    </div>
    {!! Form::close() !!}
</div>
@endsection