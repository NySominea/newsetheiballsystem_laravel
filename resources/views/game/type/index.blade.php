@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Game Type</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Game</span>
                <span class="breadcrumb-item active">Type</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.error-msg')
    @include('includes.success-msg')
    {!! Form::open(['route' => 'games.types.store','method' => 'POST']) !!}
    @csrf
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>Game Name</th>
                        <th>Min No</th>
                        <th>Max No</th>
                        <th>Start Prize (R)</th>
                        <th>5D Prize (R)</th>
                        <th>4D Prize (R)</th>
                        <th>3D Prize (R)</th>
                        <th>Order Price (R)</th>
                        <th>Addon %</th>
                        <th>Lucky Draw %</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($games) && $games->count() > 0)
                        @foreach($games as $row)
                        <tr>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->min }}</td>
                            <td>{{ $row->max }}</td>
                            <td>
                                {{Form::number("games[$row->id][start_prize]",$row->start_prize,['class' => 'w-100 form-control', 'step' => '0.01'])}}
                            </td>
                            <td>
                            {{Form::number("games[$row->id][win_prize_5n]",$row->win_prize_5d,['class' => 'w-100 form-control', 'step' => '0.01'])}}
                            </td>
                            <td>
                                {{Form::number("games[$row->id][win_prize_4n]",$row->win_prize_4d,['class' => 'w-100 form-control', 'step' => '0.01'])}}
                            </td>
                            <td>
                                {{Form::number("games[$row->id][win_prize_3n]",$row->win_prize_3d,['class' => 'w-100 form-control', 'step' => '0.01'])}}
                            </td>
                            <td>
                                {{Form::number("games[$row->id][order_price]",$row->order_price,['class' => 'w-100 form-control', 'step' => '0.01'])}}
                            </td>
                            <td>
                                {{Form::number("games[$row->id][addon_percentage]",$row->addon_percentage,['class' => 'w-100 form-control', 'step' => '0.01'])}}
                            </td>
                            <td>
                                {{Form::number("games[$row->id][lucky_draw]",$row->lucky_draw,['class' => 'w-100 form-control', 'step' => '0.01'])}}
                            </td>
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="7">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @canany(['game-type-modification'])
        @if(isset($games) && $games->count() > 0)
        <div class="card-footer">
            {!! Form::button('<i class="icon-folder mr-1"></i> '.$settings['language']['LANG_LABEL_POST'], [ 'class' => 'btn btn-success', 'type' => 'submit']) !!}
        </div>
        @endif
        @endcanany
    </div>
    {!! Form::close() !!}
</div>
@endsection