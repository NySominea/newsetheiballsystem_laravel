<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.interface.club/limitless/demo/bs4/Template/layout_1/LTR/default/full/login_simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 08 Apr 2019 05:32:19 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Sethei Lottery Company</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="/global_assets/js/main/jquery.min.js"></script>
	<script src="/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="/assets/js/app.js"></script>
	<!-- /theme JS files -->

</head>

<body>
	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">
                <div class="row">
                    <div class="col-sm-5 offset-sm-1">
                        <div class="card">
                            <div class="card-body">
								<img class="card-img-top" src="{{ asset('assets/images/logo.png') }}" alt="">
							</div>
							<a href="#"> 
								{{-- {{route('products.set','sethei-5d')}} --}}
								<div class="card-footer bg-slate-700 text-center">
									<div class="card-link text-white h4 mb-0">Sethei 5D</div>
								</div>
							</a>
                        </div>
                    </div>
                    <div class="col-sm-5 offset-sm-1">
                        <div class="card">
							<div class="card-body">
								<img class="card-img-top" src="{{ asset('assets/images/ball.png') }}" alt="">
							</div>
                            <a href="{{route('products.set','sethei-ball')}}">
                                <div class="card-footer bg-slate-700 text-center">
                                    <div class="card-link text-white h4 mb-0">Sethei Ball</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                
			</div>
			<!-- /content area -->


		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
</body>

<!-- Mirrored from demo.interface.club/limitless/demo/bs4/Template/layout_1/LTR/default/full/login_simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 08 Apr 2019 05:32:19 GMT -->
</html>
