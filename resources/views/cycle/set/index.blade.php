@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_LABEL_CYCLE_CLOSE'] }} </span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_LABEL_CYCLE_SN'] }}</span>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_LABEL_SET'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($cycle))
        {{ Form::model($cycle,['route' => ['cycles.set.update',$cycle->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'cycles.set.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    @include('includes.error-msg')
                    @include('includes.success-msg')
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Cycle SN</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-key"></i></span>
                            </span>
                            {{Form::text("cycle_sn", isset($cycle_sn) ? $cycle_sn : null,
                                ["class" => "form-control",'readonly'])
                            }}
                        </div>
                        @if($errors->has('cycle_sn'))
                            <span class="form-text text-danger">{{ $errors->first('cycle_sn') }}</span>
                        @endif
                    </div>

                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Jackpot Prize Amount ($)</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-coin-dollar"></i></span>
                            </span>
                            {{Form::text("prize", isset($prize) ? $prize : null,
                                ["class" => "form-control",'readonly'])
                            }}
                        </div>
                    </div>
                    

                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_CYCLE_DT'] }}</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("stopped_time",isset($cycle) ? date('Y-m-d H:i:s',$cycle->stopped_time) : date('Y-m-d 16:40:00',strtotime('+1 days')),
                                ["class" => "form-control datetimepicker"])
                            }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_RESULT_DT'] }} </label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("result_time",isset($cycle) ? date('Y-m-d H:i:s',$cycle->result_time) : date('Y-m-d 16:45:00',strtotime('+1 days')),
                                ["class" => "form-control datetimepicker"])
                            }}
                        </div>
                    </div>
                    
                    <div class="form-group mb-3 mb-md-2">
                        <label class="d-block font-weight-semibold">{{ $settings['language']['LANG_LABEL_STATE'] }}</label>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="1" class="form-check-input-styled-success" data-fouc checked>
                                {{ $settings['language']['LANG_LABEL_CYCLE_OPEN'] }}
                            </label>
                        </div>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="0" class="form-check-input-styled-danger" data-fouc 
                                    {{isset($cycle) && (!$cycle->state || date('Y-m-d H:i:s',strtotime('NOW')) >= date('Y-m-d H:i:s',$cycle->stopped_time)) ? 'checked' : '' }}>
                                    {{ $settings['language']['LANG_LABEL_CYCLE_STOP'] }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        @canany(['lottery-setting','lottery-operation'])
        <div class="card-footer">
            @canany(['lottery-setting'])
            {{ Form::button('<i class="icon-folder mr-1"></i> Set Cycle',['type' => 'submit', 'class' => 'btn btn-success']) }}
            @endcanany
            @canany(['lottery-operation'])
            @if(isset($cycle) && (($cycle->state && date('Y-m-d H:i:s',strtotime('NOW')) >= date('Y-m-d H:i:s',$cycle->stopped_time))) || (isset($cycle) && !$cycle->state))
                <a href="{{route('cycles.result.set')}}" class="btn btn-danger"><i class="icon-paperplane"></i> Settle Result</a>
            @endif
            @endcanany
        </div>
        @endcanany
        {{Form::hidden("prize_5d",$game->win_prize_5d)}}
        {{Form::hidden("prize_4d",$game->win_prize_4d)}}
        {{Form::hidden("prize_3d",$game->win_prize_3d)}}
        {{Form::hidden("game_type_id",$game->id)}}
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection