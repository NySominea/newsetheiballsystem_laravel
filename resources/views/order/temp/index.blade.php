@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_MENU_THIS_ORDER'] }}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none">
            @canany(['export-order-list'])
            <div class="d-flex justify-content-center">
                <button class="btn btn-link btn-primary btn-sm text-white" id="downloadExcelButton" 
                data-route="{{route('orders.temp.download')}}" 
                data-toggle="modal" data-target="#downlod_modal">
                    <i class="icon-file-excel text-white mr-1"></i> {{ $settings['language']['LANG_LABEL_DOWN'] }}
                </button>
            </div>
            @endcanany
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_THIS_ORDER'] }}</span>
                <span class="breadcrumb-item active">Current</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    {{ Form::open(['route' => 'orders.temp.index', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_START_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['begin_date']) ? $_GET['begin_date'] : date('Y-m-d 18:30:00',strtotime('-1 days'))}}" name="begin_date">
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_END_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d 17:30:00')}}" name="end_date">
            </div>
        </div>
        {{Form::submit('Query',['class' => 'btn btn-sm btn-primary mb-2 mr-2'])}}
        <a href="{{route('orders.temp.index')}}" class="btn btn-warning btn-sm mb-2">Reset</a>
    {{ Form::close() }}

    <div class="breadcrumb">
        @if(isset($topLevelUsers) && count($topLevelUsers) > 0)
            <a href="{{route('orders.temp.index')}}" class="breadcrumb-item pt-0">All</a>
            @foreach($topLevelUsers as $index => $row)
                @if($index != count($topLevelUsers) - 1 || request()->has('user_id') ) 
                    @if($row['user_type'] == 2)
                        <a href="{{route('orders.temp.index').'?level='.$row['level'].'&parent_id='.$row['id']}}" class="breadcrumb-item  pt-0">{{ getUserLevelTitle($row['level']) }} {{ $row['username'] }}</a>
                        @if($index == count($topLevelUsers) - 1 && request()->has('user_id'))
                            <span class="breadcrumb-item active pt-0">{{ getUserLevelTitle($row['level'] + 1) }} {{ $row['username'] }}</span>
                        @endif
                    @else 
                        <span class="breadcrumb-item active pt-0"> {{ $settings['language']['LANG_MENU_DIRECT_USER'] }} {{ $row['username'] }}</span>
                    @endif
                @else
                    <span class="breadcrumb-item active pt-0">{{ getUserLevelTitle($row['level']) }} {{ $row['username'] }}</span>
                @endif
            @endforeach
        @else 
            <span class="breadcrumb-item active pt-0">{{ $settings['language']['LANG_LABEL_ALL'] }}</span>
        @endif
        
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                @if(isset($showTicketDetail) && !$showTicketDetail)
                    <thead>
                        <tr class="bg-slate-800">
                            <th>{{ $settings['language']['LANG_LABEL_USERNAME'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_MEM_NUM'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_ORDER_NUM'] }}</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_AMOUNT'] }}</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_REBATE'] }}</th> 	 		
                        </tr>
                    </thead>
                    
                    <tbody>
                        @if(isset($users) && count($users) > 0)
                            @if(isset($parentHasOrder) && $parentHasOrder)
                                <tr class="font-weight-bold bg-teal-300">
                                    <td colspan="7">This User</td>
                                </tr>
                                @foreach($users as $index => $row)
                                    @if($row['userType'] == 2)
                                        @if($row['user']['id'] == request()->parent_id)
                                            <tr>
                                                <td>
                                                    @canany('view-order-list')
                                                    @if($row['hasChildren'])
                                                        <a href="{{route('orders.temp.index').'?level='.$row['user']['level'].'&parent_id='.$row['user']['id']}}">{{ $row['user']['username'] }}</a>
                                                    @else 
                                                        <a href="{{route('orders.temp.index').'?level='.$row['user']['level'].'&parent_id='.$row['user']['id'].'&user_id='.$row['user']['id']}}">{{ $row['user']['username'] }}</a>
                                                    @endif
                                                    @endcanany
                                                </td>
                                                <td> {{ $row['memberAmount'] }}</td>
                                                <td>{{ $row['orderAmount'] }}</td>
                                                <td>(R) {{ currencyFormat($row['amountR']) }}</td>
                                                <td>($) {{ currencyFormat($row['amountD']) }}</td>
                                                <td>(R) {{ currencyFormat($row['rebateR']) }}</td>
                                                <td>($) {{ currencyFormat($row['rebateD']) }}</td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                            
                            <tr class="font-weight-bold bg-teal-300">
                                <td colspan="7">{{ getUserLevelTitle(request()->level + 1) }}</td>
                            </tr>
                            @foreach($users as $index => $row)
                                @if($row['userType'] == 2)
                                    @if($row['user']['id'] != request()->parent_id || !$parentHasOrder)
                                        <tr>
                                            <td>
                                                @if($row['hasChildren'])
                                                    <a href="{{route('orders.temp.index').'?level='.$row['user']['level'].'&parent_id='.$row['user']['id']}}">{{ $row['user']['username'] }}</a>
                                                @else 
                                                    <a href="{{route('orders.temp.index').'?level='.$row['user']['level'].'&parent_id='.$row['user']['id'].'&user_id='.$row['user']['id']}}">{{ $row['user']['username'] }}</a>
                                                @endif
                                            </td>
                                            <td> {{ $row['memberAmount'] }}</td>
                                            <td>{{ $row['orderAmount'] }}</td>
                                            <td>(R) {{ currencyFormat($row['amountR']) }}</td>
                                            <td>($) {{ currencyFormat($row['amountD']) }}</td>
                                            <td>(R) {{ currencyFormat($row['rebateR']) }}</td>
                                            <td>($) {{ currencyFormat($row['rebateD']) }}</td>
                                        </tr>
                                    @endif
                                @endif
                            @endforeach

                            {{-- Direct User --}}
                            @if($hasDirectUser)
                                <tr class="font-weight-bold bg-teal-300">
                                    <td colspan="7">{{ $settings['language']['LANG_MENU_DIRECT_USER'] }}</td>
                                </tr>
                            @endif

                            @foreach($users as $row)
                                @if($row['userType'] == 1)
                                <tr>
                                    <td>
                                        @canany('view-order-list')
                                            <a href="{{route('orders.temp.index').'?level='.$row['user']['level'].'&parent_id=0&user_id='.$row['user']['id'].'&user_type=1'}}">{{ $row['user']['username'] }}</a>
                                        @endcanany
                                    </td>
                                    <td> {{ $row['memberAmount'] }}</td>
                                    <td>{{ $row['orderAmount'] }}</td>
                                    <td>(R) {{ currencyFormat($row['amountR']) }}</td>
                                    <td>($) {{ currencyFormat($row['amountD']) }}</td>
                                    <td>(R) {{ currencyFormat($row['rebateR']) }}</td>
                                    <td>($) {{ currencyFormat($row['rebateD']) }}</td>
                                </tr>
                                @endif
                            @endforeach
                            {{-- End Direct User --}}

                            @if(isset($total) && count($total) > 0)
                                <tfoot>
                                    <tr>
                                        <th>Total</th>
                                        <th>{{ $total['memberAmount'] }}</th>
                                        <th>{{ $total['orderAmount'] }}</th>
                                        <th>(R) {{ currencyFormat($total['amountR']) }}</th>
                                        <th>($) {{ currencyFormat($total['amountD']) }}</th>
                                        <th>(R) {{ currencyFormat($total['rebateR']) }}</th>
                                        <th>($) {{ currencyFormat($total['rebateD']) }}</th>
                                    </tr>
                                </tfoot>
                            @endif
                        @else 
                            <tr><td colspan="7">No Data</td></tr>
                        @endif
                    </tbody>
                @else 
                    <thead>
                        <tr class="bg-slate-800">
                            <th>{{ $settings['language']['LANG_LABEL_TICKET'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_POST_TIME'] }}</th>
                            <th>Order Bet</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_AMOUNT'] }}</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_TICKET_SUM'] }}</th> 	 		
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users['ticket'] as $ticket)
                            @foreach($ticket['orders'] as $index => $row)
                                @if($index == 0)
                                    <tr style="background-color:#eaf9eb">
                                        <td style="vertical-align: top; cursor:pointer" class="show-order-detail" data-toggle="modal" data-target="#order_detail_modal" 
                                            data-route="{{route('orders.temp.get-order-detail',$ticket['ticket'])}}">{{ $ticket['ticket'] }}</td>
                                        <td style="vertical-align: top;">{{ $ticket['created_at'] }}</td>
                                        <td>{{$row['no1']}}, {{$row['no2']}}, {{$row['no3']}}, {{$row['no4']}}, {{$row['no5']}}, {{$row['no6']}}, </td>
                                        <td>(R) {{ currencyFormat($row['amountR']) }}</td>
                                        <td>($) {{ currencyFormat($row['amountD']) }}</td>
                                        <td style="vertical-align: top;">(R) {{ currencyFormat($ticket['ticketAmountR']) }}</td>
                                        <td style="vertical-align: top;">($) {{ currencyFormat($ticket['ticketAmountD'])  }}</td>
                                    </tr>
                                @else 
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>{{$row['no1']}}, {{$row['no2']}}, {{$row['no3']}}, {{$row['no4']}}, {{$row['no5']}}, {{$row['no6']}}, </td>
                                        <td>(R) {{ currencyFormat($row['amountR']) }}</td>
                                        <td>($) {{ currencyFormat($row['amountD']) }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach
                    </tbody>
                @endif
            </table>
        </div>
    </div>
</div>
<div id="downlod_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Download Excel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body text-center p-4 h5 m-0" id="downloadModalBody">
                <i class='icon-spinner2 spinner'></i> Downloading...
            </div>
        </div>
    </div>
</div>
<div id="order_detail_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Order Detail</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="body">
                
            </div>
        </div>
    </div>
    
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection