@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Monthly Lucky Draw</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Profit Sharing</span>
                <span class="breadcrumb-item active">Lucky Draw</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>Year</th>
                        <th>Month</th>
                        <th>Amount (R)</th>
                        <th>Amount ($)</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($data) > 0)
                        @foreach($data as $year => $y)
                            @foreach($y as $month => $m)
                                <tr>
                                    <td>{{ $year }}</td>
                                    <td>{{ $month }} </td>
                                    <td>R {{ currencyFormat($m) }}</td>
                                    <td>$ {{ currencyFormat( (double)$m * $settings['exchangeRate']['rielToDollar']) }}</td>
                                </tr>
                            @endforeach
                        @endforeach
                    @else 
                        <tr><td colspan="4">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection