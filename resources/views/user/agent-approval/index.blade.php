@extends('layouts.master')
@section('custom-css')
    <style>
        table a{
            color: #333;
        }
        table a:hover{
            color: #111;
        }
    </style>
@endsection
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_MENU_AGENT_APPLY'] }}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_USER_LIST'] }}</span>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_AGENT_APPLY'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    {{ Form::open(['route' => 'accounts.recharges.index', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_STATE'] }}</div>
                </div>
                {{Form::select("state",['0' => 'All',Account::APPROVED => 'Approved', Account::UNAPPROVED => 'Unapproved', Account::NOT_APPROVED => 'Not Approved'], 
                isset($_GET['state']) ? $_GET['state'] : null,["class" => "form-control input-sm"])}}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_USERNAME'] }}</div>
                </div>
                {{Form::text("username",old("username") ? old("username") : (isset($_GET['username']) ? $_GET['username'] : null),["class" => "form-control input-sm", "placeholder" => 'Enter user name'])}}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_START_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['begin_time']) ? $_GET['begin_time'] : date('Y-m-d 00:00:00',strtotime('-1 month'))}}" name="begin_time">
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_END_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['end_time']) ? $_GET['end_time'] : date('Y-m-d 00:00:00')}}" name="end_time">
            </div>
        </div>
        {{Form::submit('Search',['class' => 'btn btn-sm btn-primary mb-2 mr-2'])}}
        <a href="{{route('accounts.recharges.index')}}" class="btn btn-warning btn-sm mb-2">Reset</a>
    {{ Form::close() }}

    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>{{ $settings['language']['LANG_LABEL_USERNAME'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_NO'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_DISTRICT'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_SALE_NAME'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_SALE_PHONE'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_SALE_MANAGER'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_DATETIME'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_AUDITOR'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_APPROVE_TIME'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_STATE'] }}</th>
                        <th>Action</th>			
                    </tr>
                </thead>
                <tbody>
                    @if(isset($users) && $users->count() > 0)
                        @foreach($users as $key => $row)
                            <tr>
                                @if($row->under_users > 0)
                                    <td><a href="{{route('users.sale-network.index').'?level='.$row->level.'&parent_id='.$row->id}}">{{ $row->username }}</a></td>
                                    <td><a href="{{route('users.sale-network.index').'?level='.$row->level.'&parent_id='.$row->id}}">{{ $row->account_number }}</a></td>
                                @else 
                                    <td>{{ $row->username }}</td>
                                    <td>{{ $row->account_number }}</td>
                                @endif
                                <td>{{ $row->account_balance }}</td>
                                <td>{{ $row->account_frozen }}</td>
                                <td>{{ $row->commission }}</td>
                                <td>{{ $row->current_commission }}</td>
                                <td>{{ $row->win_money }}</td>
                                <td class="group-btn-action">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2 dropdown-toggle mr-1" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog5"></i> Action</button>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(135px, 36px, 0px);">
                                          @cannany('user-account-modification')
                                          <a href="{{route('users.sale-network.edit',$row->id)}}" class="dropdown-item"><i class="icon-pen6"></i> {{ $settings['language']['LANG_LABEL_SETTING'] }}</a>
                                          @endcanany
                                            <div class="dropdown-divider m-0"></div>
                                            <a href="#" class="dropdown-item"><i class="icon-coin-dollar"></i> {{ $settings['language']['LANG_LABEL_RECHARGE'] }}</a>
                                            <div class="dropdown-divider m-0"></div>
                                            <a href="#" class="dropdown-item"><i class="icon-coin-dollar"></i> {{ $settings['language']['LANG_MENU_TAKE_CASH'] }}</a>
                                            <div class="dropdown-divider m-0"></div>
                                            @canany('view-user-account')
                                            <a href="{{route('users.accounts.index',$row->id)}}" class="dropdown-item"><i class="icon-credit-card"></i> {{ $settings['language']['LANG_LABEL_ACCOUNT'] }}</a>
                                            @endcanany
                                            <div class="dropdown-divider m-0"></div>
                                            @canany('view-user-account-log')
                                                <a href="{{route('users.logs.index',$row->id)}}" class="dropdown-item"><i class="icon-list"></i> {{ $settings['language']['LANG_LABEL_ACCOUNT_LOG'] }}</a>
                                            @endcanany
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="10">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(isset($users) && count($users) > 0)
        <div class="card-footer">
            @if($users->hasMorePages())
                <div class="mb-2">
                    {!! $users->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$users->firstItem()}} to {{$users->lastItem()}}
                of  {{$users->total()}} entries
            </div>
        </div>
        @endif
    </div>
    
</div>
@endsection

@section('custom-js')
<script>
    $(document).ready(function(){
        // $('.table-responsive').on('show.bs.dropdown', function () {
        //     $('.table-responsive').css( "overflow", "inherit" );
        // });

        // $('.table-responsive').on('hide.bs.dropdown', function () {
        //     $('.table-responsive').css( "overflow", "auto" );
        // })
        if($('#state').length > 0){
            $('#state').change(function(){ 
                window.location = replaceUrlParam(window.location.href,'state',$('#state').val());
            });
        }
        if($('.appendQueryBtn').length > 0){
            $('.appendQueryBtn').click(function(){
                $(this).parents('form').submit(function(e){
                    e.preventDefault(); 
                }); 
                window.location = replaceUrlParam(window.location.href,'keyword',$('#keyword').val());
            })
        }
        function replaceUrlParam(url, paramName, paramValue)
        {
            if (paramValue == null) {
                paramValue = '';
            }
            var pattern = new RegExp('\\b('+paramName+'=).*?(&|#|$)');
            if (url.search(pattern)>=0) {
                return url.replace(pattern,'$1' + paramValue + '$2');
            }
            url = url.replace(/[?#]$/,'');
            return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;
        }
    })
</script>
@endsection