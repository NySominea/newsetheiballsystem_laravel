<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use DB;
use App\Model\GameRebate as gameRebateModel;
use App\Model\GameType;

class GameRebate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:gameRebate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_ball_mysql')->table('game_rebates')->orderBy('id','Asc')->chunk(10, function($gameRebates){
            $data = [];
            foreach($gameRebates as $gameRebate){
                $data [] = [
                    'id' => $gameRebate->id,
                    'l1_rebate' => $gameRebate->l1_rebate,
                    'l2_rebate' => $gameRebate->l2_rebate,
                    'l3_rebate' => $gameRebate->l3_rebate,
                    'l4_rebate' => $gameRebate->l4_rebate,
                    'l5_rebate' => $gameRebate->l5_rebate,
                    'l6_rebate' => $gameRebate->l6_rebate,
                    'l7_rebate' => $gameRebate->l7_rebate,
                    'l8_rebate' => $gameRebate->l8_rebate,
                    'game_type_id' => $gameRebate->game_type_id,
                    'root_id' => $gameRebate->root_id,
                    'created_at' => $gameRebate->created_at,
                    'updated_at' => $gameRebate->updated_at
                ];
            }
            gameRebateModel::insert($data);
        });

        $columns = [
            'l1_rebate',
            'l2_rebate',
            'l3_rebate',
            'l4_rebate',
            'l5_rebate',
            'l6_rebate',
            'l7_rebate',
            'l8_rebate',
            'root_id',
            'game_type_id',
            'created_at',
            'updated_at'
        ];
        $rebates[2563] = [0.3,0.25,0.25,0.5,0.5,0.75,1,7];
        $rebates[2730] = [0.3,0.25,0.25,0.5,0.75,0.5,1,7];
        $rebates[2444] = [0.5,0.75,1,1,0,0,0,0];
        $game = GameType::whereCode('L101')->first();
        foreach($rebates as $id => $r){
            $rebate = $r;
            array_push($rebate,$id,$game ? $game->id : 1,date('Y-m-d H:i:s'),date('Y-m-d H:i:s'));
            $inserted_data[] = $rebate;
        }
        
        \Batch::insert(new gameRebateModel(), $columns, $inserted_data);
    }
}
