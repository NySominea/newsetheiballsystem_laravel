<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use App\Model\Cycle as CycleModel;
use DB;

class Cycle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:cycle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 'id',
        // 'cycle_sn',
        // 'state',
        // 'has_released',
        // 'stopped_time',
        // 'result_time',
        // 'result_number',
        // 'is_jackpot',
        // 'prize',
        // 'prize_5d',
        // 'prize_4d',
        // 'prize_3d',
        // 'lucky_draw',
        // 'game_type_id'
        DB::connection('sethei_ball_mysql')->table('cycles')->orderBy('id','asc')
            ->chunk(200,function($cycles){
                $data = [];
                foreach($cycles as $cycle){
                    $data[] = [
                        'id' => $cycle->id,
                        'cycle_sn' => $cycle->cycle_sn,
                        'state' => $cycle->state,
                        'has_released' => $cycle->has_released,
                        'stopped_time' => $cycle->stopped_time,
                        'result_time' => $cycle->result_time,
                        'result_number' => $cycle->result_number,
                        'is_jackpot' => $cycle->is_jackpot,
                        'prize' => $cycle->prize,
                        'prize_5d' => $cycle->prize_5d,
                        'prize_4d' => $cycle->prize_4d,
                        'prize_3d' => $cycle->prize_3d,
                        'lucky_draw' => $cycle->lucky_draw,
                        'game_type_id' => $cycle->game_type_id,
                        'created_at' => $cycle->created_at,
                        'updated_at' => $cycle->updated_at
                    ];
                }
                CycleModel::insert($data);
            });
    }
}
