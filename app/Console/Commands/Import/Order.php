<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use DB;
use App\Model\Order as OrderModel;


class Order extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_ball_mysql')->table('orders')->orderBy('id','asc')
        // ->where('id', '1')
        ->chunk(1000, function($orders){
            foreach($orders as $order){
                $data[] = [
                    'id' => $order->id,
                    'ticket' => $order->ticket,	
                    'is_settle' => $order->is_settle,
                    'is_win' => $order->is_win,
                    'win_prize' => $order->win_prize,
                    'win_number' => $order->win_number,
                    'win_amount' => $order->win_amount,
                    'amount' => $order->bet_amount,
                    'no1' => $order->no1,
                    'no2' => $order->no2,
                    'no3' => $order->no3,
                    'no4' => $order->no4,
                    'no5' => $order->no5,
                    'no6' => $order->no6,	
                    'l1_id' => $order->l1_id,
                    'l2_id' => $order->l2_id,
                    'l3_id' => $order->l3_id,
                    'l4_id' => $order->l4_id,
                    'l5_id' => $order->l5_id,
                    'l6_id' => $order->l6_id,
                    'l7_id' => $order->l7_id,
                    'l8_id' => $order->l8_id,
                    'l1_rebate' => $order->l1_rebate,
                    'l2_rebate' => $order->l2_rebate,
                    'l3_rebate' => $order->l3_rebate,
                    'l4_rebate' => $order->l4_rebate,
                    'l5_rebate' => $order->l5_rebate,
                    'l6_rebate' => $order->l6_rebate,
                    'l7_rebate' => $order->l7_rebate,
                    'l8_rebate' => $order->l8_rebate,
                    'l1_du' => $order->l1_du,
                    'l2_du' => $order->l2_du,
                    'l3_du' => $order->l3_du,
                    'l4_du' => $order->l4_du,
                    'l5_du' => $order->l5_du,
                    'l6_du' => $order->l6_du,
                    'l7_du' => $order->l7_du,
                    'l8_du' => $order->l8_du,
                    'user_type' => $order->user_type,
                    'parent_id' => $order->parent_id,
                    'state' => $order->state,
                    'cycle_id' => $order->cycle_id,
                    'user_id' => $order->user_id,
                    'game_type_id' => $order->game_type_id,
                    'created_at' => $order->created_at,
                    'updated_at' => $order->updated_at
                ];
            }
            orderModel::insert($data);
        });
    }
}
