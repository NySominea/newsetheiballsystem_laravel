<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use App\Model\GameType as modelGameType; 
use DB;
class GameType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:gameType';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        DB::connection('sethei_ball_mysql')->table('game_types')->orderBy('id','asc')->chunk(10,function($gameTypes){
            $data = [];
            foreach($gameTypes as $gameType){
                $data[] = [
                    'id' => $gameType->id,
                    'code'  => $gameType->code, 
                    'name' => $gameType->name,
                    'min' => $gameType->min,
                    'max' => $gameType->max,
                    'start_prize' => $gameType->start_prize,
                    'current_prize' => $gameType->current_prize,
                    'win_prize_5d' => $gameType->win_prize_5d,
                    'win_prize_4d' => $gameType->win_prize_4d,
                    'win_prize_3d' => $gameType->win_prize_3d,
                    'addon_percentage' => $gameType->addon_percentage,
                    'order_price' => $gameType->order_price,
                    'lucky_draw' => $gameType->lucky_draw,
                    'created_at' => $gameType->created_at,
                    'updated_at' => $gameType->updated_at,
                ];
            }
           // dd($data);
            modelGameType::insert($data);
        });
    }
}
