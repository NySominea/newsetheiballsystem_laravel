<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use App\Constants\User5dAccount;

class UserAccount5D extends Model
{
    protected $table = "d_user_accounts";

    protected $fillable = [
        'id',
        'title',
        'number',
        'name',
        'balance',
        'frozen',
        'state',
        'sort',
        'user_id',
        'category_id',
        'type_id'
    ];
    
    public static function createCashAccount($userId = null){
        $userId = $userId ?: auth()->id();
        return UserAccount5D::create([
            'title' => 'Default Account',
            'number' => 'Default Account',
            'name' => 'Default Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => User5dAccount::CASH_ACCOUNT
        ]);
    }
    public static function createBonusAccount($userId = null){
        $userId = $userId ?: auth()->id();
        return UserAccount5D::create([
            'title' => 'Bonus Account',
            'number' => 'Bonus Account',
            'name' => 'Bonus Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => User5dAccount::BONUS_ACCOUNT
        ]);
    }
    public static function createProfitAccount($userId = null){
        $userId = $userId ?: auth()->id();
        return UserAccount5D::create([
            'title' => 'Profit Account',
            'number' => 'Profit Account',
            'name' => 'Profit Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => User5dAccount::PROFIT_ACCOUNT
        ]);
    }
}
