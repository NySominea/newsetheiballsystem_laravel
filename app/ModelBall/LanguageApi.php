<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LanguageApi extends Model
{
    protected $fillable = [
        'id',
        'module',
        'english',
        'khmer',
        'chinese',
        'label',
        'abstract'
    ];
}
