<?php 
use App\Model\Currency;
use App\Model\Exchange;
use App\Model\Language;
use App\Model\LanguageAdmin;

function pushNotification($title,$body){
    $pushNotifications = notificationInstance();
    $payload = [
        'title' => $title,
        'description' => $body,
        'channel_id' => str_random(10)
    ];

    $publishResponse = $pushNotifications->publishToInterests(
    ["result"],
    [
        "fcm" => [
            'data' => ['result' => json_encode($payload)],
            "notification" => [
                "title" => $title,
                "body" => $body,
            ],
        ],
    ]
    );
}

function pushNotificationToUser($title,$body,$userIds = []){
    $beamsClient = notificationInstance();
    $payload = [
        'title' => $title,
        'description' => $body,
        'channel_id' => str_random(10)
    ];
    $publishResponse = $beamsClient->publishToUsers(
        $userIds,
        array(
          "fcm" => array(
            'data' => ['result' => json_encode($payload)],
            "notification" => array(
              "title" => $title,
              "body" => $body
            )
          )
    ));
}

function notificationInstance(){
    return new \Pusher\PushNotifications\PushNotifications(array(
        "instanceId" => "a9c53072-ab8d-4ff5-9950-3ba6c0a95651",
        "secretKey" => "E9817E476B7951FFF4B450D715E3D93515383D0A744AC0F79DF899D341A6AFA1",
    ));
}

function getFrontendGeneralSetting(){ 
    // dd('do');dd(\DB::connection('sethei_mysql')->table('tf_currency')->get());
    $settings = [];
    $settings['exchangeRate'] = [
        'dollarToRiel' => exchangeRateFromDollarToRiel(),
        'rielToDollar' => exchangeRateFromRielToDollar(),
    ];
    $settings['language'] = getAdminLanguageData();
    $settings['avaiableLanguage'] = getAvailableLanguage();
    return $settings;
}

function getAvailableLanguage(){
    $language = Language::orderBy('sort','ASC')->get()->keyBy('language_field');
    return $language;
}
function getAdminLanguageData(){ 
    $currentLang = 'English';
    if(app()->getLocale() == 'en'){
        $currentLang = 'english';
    }elseif(app()->getLocale() == 'zh'){
        $currentLang = 'chinese';
    }elseif(app()->getLocale() == 'kh'){
        $currentLang = 'khmer';
    }  
    return LanguageAdmin::all()->pluck($currentLang,'label')->toArray();
}

function api_trans($msg,$key = []){
    return trans($msg,$key, getRequestLanguage());
}
function getRequestLanguage(){
    $lang = request()->header('Accept-Language') ?? 'en';
    return $lang;
}

function getUserLevelTitle($level){
    $languages = getAdminLanguageData();
    $title = $languages['LANG_LABEL_L1_TITLE'];
    switch($level){
        case 1: $title = $languages['LANG_LABEL_L1_TITLE']; break;
        case 2: $title = $languages['LANG_LABEL_L2_TITLE']; break;
        case 3: $title = $languages['LANG_LABEL_L3_TITLE']; break;
        case 4: $title = $languages['LANG_LABEL_L4_TITLE']; break;
        case 5: $title = $languages['LANG_LABEL_L5_TITLE']; break;
        case 6: $title = $languages['LANG_LABEL_L6_TITLE']; break;
        case 7: $title = $languages['LANG_LABEL_L7_TITLE']; break;
        case 8: $title = $languages['LANG_LABEL_L8_TITLE']; break;
        case 9: $title = $languages['LANG_LABEL_THISUSER']; break;
    }
    return $title;
}

function currencyFormat($amount,$decimal = 2){
    return number_format($amount, $decimal);
}
function stringToDouble($str){
    return doubleval(str_replace(',','',$str));
}
function exchangeRateFromRielToDollar(){
    $currency = Exchange::where('from_currency',1)->first();
    return $currency ? 1 / $currency->rate : 0;
}
function exchangeRateFromDollarToRiel(){
    $currency = Exchange::where('from_currency',1)->first();
    return $currency ? $currency->rate : 0;
}

function addPrefixStringPad($input,$length,$string){
    return str_pad($input, $length, $string, STR_PAD_LEFT);
}
function addSuffixStringPad($input,$length,$string){
    return str_pad($input, $length, $string, STR_PAD_RIGHT);
}

function permissions(){
    return [
        'en' => [
            'user' => [
                'module' => 'User',
                'permissions' => [
                    ['text' => 'View User Account', 'value' => 'view-user-account'], 
                    ['text' => 'Add New User Account', 'value' => 'add-new-user-account'], 
                    ['text' => 'User Account Modification', 'value' => 'user-account-modification'],
                    ['text' => 'User Recharge', 'value' => 'user-recharge'],
                    ['text' => 'View User Account Log', 'value' => 'view-user-account-log'],
                    ['text' => 'User Take Cash', 'value' => 'user-take-cash']
                ]
            ],
            'order' => [
                'module' => 'Order List',
                'permissions' => [
                    ['text' => 'View Order List', 'value' => 'view-order-list'], 
                    ['text' => 'Export Order List', 'value' => 'export-order-list']
                ]
            ],
            'report' => [
                'module' => 'History Report',
                'permissions' => [
                    ['text' => 'View History Report', 'value' => 'view-history-report'], 
                    ['text' => 'Export History Report', 'value' => 'export-history-report']
                ]
            ],
            'finance' => [
                'module' => 'Finance',
                'permissions' => [
                    ['text' => 'View Transaction Log', 'value' => 'view-transaction-log'],
                    ['text' => 'Download Transaction Log', 'value' => 'download-transaction-log'],  
                    ['text' => 'View Platform Account', 'value' => 'view-platform-account'],
                    ['text' => 'Platform Account Access Transfer', 'value' => 'platform-account-access-transfer'],
                    ['text' => 'Add New Account', 'value' => 'add-new-account'],
                    ['text' => 'Account Modification', 'value' => 'account-modification'],
                    ['text' => 'Add Class Manager', 'value' => 'add-class-manager'],
                    
                ]
            ],
            'cycle' => [
                'module' => 'Cycle Result',
                'permissions' => [
                    ['text' => 'Lottery Setting', 'value' => 'lottery-setting'], 
                    ['text' => 'Lottery Operation', 'value' => 'lottery-operation'],
                    ['text' => 'Billing Operation', 'value' => 'billing-operation'], 
                    ['text' => 'View Lottery Log', 'value' => 'view-lottery-log']
                ]
            ],
            'profit' => [
                'module' => 'Profit Sharing',
                'permissions' => [
                    ['text' => 'Monthly Lucky Draw', 'value' => 'lucky-draw'],
                ]
            ],
            'manager' => [
                'module' => 'Manager',
                'permissions' => [
                    ['text' => 'View Manager Account', 'value' => 'view-manager-account'], 
                    ['text' => 'Add New Manager Account', 'value' => 'add-new-manager-account'], 
                    ['text' => 'Manager Account Modification', 'value' => 'manager-account-modification'],
                    ['text' => 'View Role', 'value' => 'view-role'], 
                    ['text' => 'Add New Role', 'value' => 'add-new-role'],
                    ['text' => 'Role Modification', 'value' => 'role-modification'],
                    ['text' => 'View Manager Log', 'value' => 'view-manager-log']
                ]
            ],
            'game' => [
                'module' => 'Game Set',
                'permissions' => [
                    ['text' => 'View Game Type Setting', 'value' => 'view-game-type-setting'],
                    ['text' => 'Game Type Modification', 'value' => 'game-type-modification'],
                    ['text' => 'View Game Rebate Setting', 'value' => 'view-game-rebate-setting'],
                    ['text' => 'Game Rebate Modification', 'value' => 'game-rebate-modification'],
                    ['text' => 'View Game Rebate Setting', 'value' => 'view-game-promotion'],
                    ['text' => 'Game Rebate Modification', 'value' => 'game-promotion-modification']
                ]
            ],
            'system' => [
                'module' => 'System Set',
                'permissions' => [
                    ['text' => 'View Language Set', 'value' => 'view-language-set'],
                    ['text' => 'Language Set Modification', 'value' => 'language-set-modification'],
                    ['text' => 'View Exchange Rate Set', 'value' => 'view-exchange-rate-set'],
                    ['text' => 'Exchange Rate Modification', 'value' => 'exchange-rate-modification'],
                    ['text' => 'View Location Set', 'value' => 'view-location-set'],
                    ['text' => 'Location Set Modification', 'value' => 'location-set-modification']
                ]
            ],
            'analysis' => [
                'module' => 'Analysis',
                'permissions' => [
                    ['text' => 'Analysis Operation', 'value' => 'analysis-operation']
                ]
            ]
        ],
    ];
}


function echoTime($title){
    echo $title." ".date('Y-m-d-H:i:s.').preg_replace("/^.*\./i","", microtime(true))."<br>";
}