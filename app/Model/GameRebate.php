<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GameRebate extends Model
{
    protected $table = "ball_game_rebates";
    protected $fillable = [
        'id',
        'l1_rebate',	
        'l2_rebate',
        'l3_rebate',
        'l4_rebate',
        'l5_rebate',
        'l6_rebate',
        'l7_rebate',
        'l8_rebate',
        'game_type_id',
        'root_id'
    ];

    public function gameType(){
        return $this->belongsTo(GameType::class);
    }
}
