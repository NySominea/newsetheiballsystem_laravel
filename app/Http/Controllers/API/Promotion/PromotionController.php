<?php

namespace App\Http\Controllers\API\Promotion;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Model\Promotion;

class PromotionController extends BaseController
{
    public function getPromotionInfo(){
        $now = time();
        $data = null;
        $promotion = Promotion::where('from_date','<=',$now)->where('to_date','>=',$now)->first();
        if($promotion){
            $data = [
                'target_order' => $promotion->target_order,
                'bonus_order' => $promotion->bonus_order,
                'running_text' => $promotion->running_text 
            ];
        }
        return $this->sendResponse($data, 'OK');
    }
}
