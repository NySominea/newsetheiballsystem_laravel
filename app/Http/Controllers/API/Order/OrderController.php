<?php

namespace App\Http\Controllers\API\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Model\Cycle;
use App\Model\Order;
use App\Model\OrderTemp;
use App\Model\GameType;
use App\Model\GameRebate;
use App\Model\Promotion;
use Illuminate\Pagination\LengthAwarePaginator;
use App\User;
use DB;
use App\Model\UserAccount5D;


class OrderController extends BaseController
{
    public function createOrder(){
        ini_set('max_execution_time', 300);
        $now = time();
        $s = microtime(true); 
        // Check order content
        $orders = request()->get('data') ?? [];
        if(count($orders) <= 0) 
            return $this->sendError(api_trans('cycle.no_order'));
        
        // Check authentication
        $user = auth()->user()->load('ballCashAccount','ballBonusAccount');
        if(!$user || $user->state != 1) 
            return $this->sendError(api_trans('auth.permission_denied'));
        
        // Check active cycle
        $cycle = Cycle::whereStateAndHasReleased(1,0)->first();
        if(!$cycle) 
            return $this->sendError(api_trans('cycle.no_cycle'));
        
        // Check cycle stop time
        if($cycle->stopped_time < strtotime("NOW")) 
            return $this->sendError(api_trans('cycle.cycle_stop'));

        // Check game type config
        $game = GameType::whereCode('L101')->first();
        if(!$game) 
            return $this->sendError(api_trans('cycle.no_config'));

        // Check valid bet number
        if(!$this->checkValidBetNumber($orders,$game)) 
            return $this->sendError(api_trans('cycle.out_of_range_bet'));
        
        $promotion = Promotion::where('from_date','<=',$now)->where('to_date','>=',$now)->first();

        // Check if balance is sufficient
        if(!$this->checkSufficientBalance($user,$orders,$promotion,$game))
            return $this->sendError(api_trans('account.insufficient_balance'));
        
        
        $rebate = $this->getGameRebate($user,$game);
        $ticket = $this->generateTicketNumber($cycle->cycle_sn,$user->id);
        $orderTemps = [];
        $totalAmount = 0;
        $now = date('Y-m-d H:i:s');
        
        DB::beginTransaction();
        try{
            // IF Promotion valid
            if($promotion && count($orders) > $promotion->target_order){
                $numberOfBonusOrders =  $this->getNumberOfBonusOrders(count($orders),$promotion);
                $paidOrders = array_slice($orders, 0, count($orders) - $numberOfBonusOrders);
                $nonPaidOrders = array_slice($orders, count($orders) - $numberOfBonusOrders, count($orders)); 
                
                foreach($paidOrders as $order){
                    $orderTemps[] = [
                        'ticket' => $ticket,
                        'cycle_id' => $cycle->id,
                        'user_id' => $user->id,
                        'amount' => $game->order_price,
                        'user_type' => $user->user_type,
                        'parent_id' => $user->parent_id,
                        'game_type_id' => $game->id,
                        'no1' => addPrefixStringPad($order[0], 2, '0'),
                        'no2' => addPrefixStringPad($order[1], 2, '0'),
                        'no3' => addPrefixStringPad($order[2], 2, '0'),
                        'no4' => addPrefixStringPad($order[3], 2, '0'),
                        'no5' => addPrefixStringPad($order[4], 2, '0'),
                        'no6' => addPrefixStringPad($order[5], 2, '0'),
                        'l1_id'=> $user->level == 1 ? $user->id : $user->l1_id,
                        'l2_id'=> $user->level == 2 ? $user->id : $user->l2_id,
                        'l3_id'=> $user->level == 3 ? $user->id : $user->l3_id,
                        'l4_id'=> $user->level == 4 ? $user->id : $user->l4_id,
                        'l5_id'=> $user->level == 5 ? $user->id : $user->l5_id,
                        'l6_id'=> $user->level == 6 ? $user->id : $user->l6_id,
                        'l7_id'=> $user->level == 7 ? $user->id : $user->l7_id,
                        'l8_id'=> $user->level == 8 ? $user->id : $user->l8_id,
                        'l1_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l1_rebate / 100 * $game->order_price,
                        'l2_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l2_rebate / 100 * $game->order_price,
                        'l3_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l3_rebate / 100 * $game->order_price,
                        'l4_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l4_rebate / 100 * $game->order_price,
                        'l5_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l5_rebate / 100 * $game->order_price,
                        'l6_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l6_rebate / 100 * $game->order_price,
                        'l7_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l7_rebate / 100 * $game->order_price,
                        'l8_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l8_rebate / 100 * $game->order_price,
                        'created_at' => $now
                    ];
                    $totalAmount+=$game->order_price;
                }

                foreach($nonPaidOrders as $order){
                    $orderTemps[] = [
                        'ticket' => $ticket,
                        'cycle_id' => $cycle->id,
                        'user_id' => $user->id,
                        'amount' => 0,
                        'user_type' => $user->user_type,
                        'parent_id' => $user->parent_id,
                        'game_type_id' => $game->id,
                        'no1' => addPrefixStringPad($order[0], 2, '0'),
                        'no2' => addPrefixStringPad($order[1], 2, '0'),
                        'no3' => addPrefixStringPad($order[2], 2, '0'),
                        'no4' => addPrefixStringPad($order[3], 2, '0'),
                        'no5' => addPrefixStringPad($order[4], 2, '0'),
                        'no6' => addPrefixStringPad($order[5], 2, '0'),
                        'l1_id'=> $user->level == 1 ? $user->id : $user->l1_id,
                        'l2_id'=> $user->level == 2 ? $user->id : $user->l2_id,
                        'l3_id'=> $user->level == 3 ? $user->id : $user->l3_id,
                        'l4_id'=> $user->level == 4 ? $user->id : $user->l4_id,
                        'l5_id'=> $user->level == 5 ? $user->id : $user->l5_id,
                        'l6_id'=> $user->level == 6 ? $user->id : $user->l6_id,
                        'l7_id'=> $user->level == 7 ? $user->id : $user->l7_id,
                        'l8_id'=> $user->level == 8 ? $user->id : $user->l8_id,
                        'l1_rebate'=> 0,
                        'l2_rebate'=> 0,
                        'l3_rebate'=> 0,
                        'l4_rebate'=> 0,
                        'l5_rebate'=> 0,
                        'l6_rebate'=> 0,
                        'l7_rebate'=> 0,
                        'l8_rebate'=> 0,
                        'created_at' => $now
                    ];
                }
            }
            // Promotion invalid
            else{
                foreach($orders as $order){
                    $orderTemps[] = [
                        'ticket' => $ticket,
                        'cycle_id' => $cycle->id,
                        'user_id' => $user->id,
                        'amount' => $game->order_price,
                        'user_type' => $user->user_type,
                        'parent_id' => $user->parent_id,
                        'game_type_id' => $game->id,
                        'no1' => addPrefixStringPad($order[0], 2, '0'),
                        'no2' => addPrefixStringPad($order[1], 2, '0'),
                        'no3' => addPrefixStringPad($order[2], 2, '0'),
                        'no4' => addPrefixStringPad($order[3], 2, '0'),
                        'no5' => addPrefixStringPad($order[4], 2, '0'),
                        'no6' => addPrefixStringPad($order[5], 2, '0'),
                        'l1_id'=> $user->level == 1 ? $user->id : $user->l1_id,
                        'l2_id'=> $user->level == 2 ? $user->id : $user->l2_id,
                        'l3_id'=> $user->level == 3 ? $user->id : $user->l3_id,
                        'l4_id'=> $user->level == 4 ? $user->id : $user->l4_id,
                        'l5_id'=> $user->level == 5 ? $user->id : $user->l5_id,
                        'l6_id'=> $user->level == 6 ? $user->id : $user->l6_id,
                        'l7_id'=> $user->level == 7 ? $user->id : $user->l7_id,
                        'l8_id'=> $user->level == 8 ? $user->id : $user->l8_id,
                        'l1_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l1_rebate / 100 * $game->order_price,
                        'l2_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l2_rebate / 100 * $game->order_price,
                        'l3_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l3_rebate / 100 * $game->order_price,
                        'l4_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l4_rebate / 100 * $game->order_price,
                        'l5_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l5_rebate / 100 * $game->order_price,
                        'l6_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l6_rebate / 100 * $game->order_price,
                        'l7_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l7_rebate / 100 * $game->order_price,
                        'l8_rebate'=> $user->is_test || $user->com_direct == 1 ? 0 : $rebate->l8_rebate / 100 * $game->order_price,
                        'created_at' => $now
                    ];
                    $totalAmount+=$game->order_price;
                }
            }
            
            // Save Order and Commission
            if(OrderTemp::insert($orderTemps)){
                if($user->dBonusAccount && $user->dBonusAccount->balance > 0){
                    if($user->dBonusAccount->balance > $totalAmount){
                        $user->dBonusAccount->decrement('balance',$totalAmount);
                    }else{
                        $remainAmount = $totalAmount - $user->dBonusAccount->balance;
                        $user->dBonusAccount->update(['balance' => 0]);
                        $user->dCashAccount->decrement('balance',$remainAmount);
                    }
                }else{
                    $user->dCashAccount->decrement('balance',$totalAmount);
                }
                
                if(!$user->is_test){
                    $userIds = [];
                    if($user->com_direct == 2){
                        for($i=1;$i<=$user->level;$i++){
                            $userIds[] = $orderTemps[0]['l'.$i.'_id'];
                        }
                        $users = User::whereIn('id',$userIds)->get()->keyBy('id');
                        foreach($users as $id => $u){
                            $u->increment('current_commission_ball',$totalAmount * $rebate->{'l'.$u->level.'_rebate'} / 100);
                        }
                    }
                    
                    $cycle->increment('prize',$totalAmount * $game->addon_percentage / 100);
                    $cycle->increment('lucky_draw',$totalAmount * $game->lucky_draw / 100);
                }
                
            }
            
            DB::commit();

            // $pusher = new \Pusher\Pusher("e39a4e096510340c985b", "b2ec5375407bbe9e618e", "797571", array('cluster' => 'ap1'));
            // $pusher->trigger('sethei-ball-cycle', 'jackpot-increase', array('amount' => $cycle->prize * exchangeRateFromRielToDollar()));
            // $pusher->trigger('sethei-ball-cycle', 'lucky-draw-increase', array('amount' => $cycle->lucky_draw * exchangeRateFromRielToDollar()));


            return $this->sendResponse(['username' => $user->username, 'account_no' => $user->account_number,
                                        'date' => date('Y-m-d H:i A',strtotime($now)), 
                                        'amount' => currencyFormat($totalAmount)." R",
                                        'ticket' => $ticket, 'result_time' => date('Y-m-d H:i A',$cycle->result_time), 
                                        'balance' => currencyFormat($user->ballCashAccount->balance)." R",
                                        'orders' => $orders,
                                        'promotion' => $promotion ? $promotion->running_text : null], 'OK');
        }catch(Exception $exception){
            DB::rollback();
            return $this->sendError(api_trans('cycle.failed_to_order'));
        }
        
    }

    public function getTicketsByCycle($cycle_id){
        $cycle = $cycle_id ? Cycle::find($cycle_id) : null;

        if(!$cycle) return  $this->sendError(api_trans('cycle.no_cycle'));

        $user_id = request()->user_id ?: auth()->id();
        if($cycle->has_released && !$cycle->state){ 
            $tickets = Order::selectRaw('count(*) as qty, sum(amount) as amount, sum(win_amount) as win_money, ticket, created_at')
                                ->whereCycleIdAndUserId($cycle->id,$user_id)
                                ->groupBy('ticket','created_at')->orderBy('created_at','DESC')
                                ->paginate(15)
                                ->toArray();
        }else{
            $tickets = OrderTemp::selectRaw('count(*) as qty, sum(amount) as amount, sum(win_amount) as win_money, ticket, created_at')
                                    ->whereCycleIdAndUserId($cycle->id,$user_id)
                                ->groupBy('ticket','created_at')->orderBy('created_at','DESC')
                                ->paginate(15)
                                ->toArray();
        }

        if(!$tickets) return  $this->sendError(api_trans('cycle.no_ticket'));
        
        $tickets['tickets'] = $tickets['data'];
        unset($tickets['data']);

        foreach($tickets['tickets'] as $i => $ticket){
            $tickets['tickets'][$i]['amount'] = currencyFormat( $ticket['amount']);
            $tickets['tickets'][$i]['win_money'] = $ticket['win_money'];
            $tickets['tickets'][$i]['created_at'] = date('Y-m-d H:i A',strtotime($ticket['created_at']));
        }

        return $this->sendResponse($tickets, 'OK');
    }

    public function getOrdersByTicket($ticket){
        $is_settle = request()->get('is_settle');
        
        if(!$ticket && !$is_settle){
            return  $this->sendError(api_trans('cycle.no_ticket'));
        }

        $orders = $is_settle 
                        ? Order::where(['ticket' => $ticket])->with('user', 'cycle')->get() 
                        : OrderTemp::where(['ticket' => $ticket])->with('user', 'cycle')->get();
        
        if($orders->count() <= 0){
            return  $this->sendError(api_trans('cycle.no_order'));
        }

        $ordersFormat = [];
        foreach($orders as $order){
            $ordersFormat[] = [
                'numbers' => json_encode([$order->no1,$order->no2,$order->no3,$order->no4,$order->no5,$order->no6]),
                'win_amount' => ((string) $order->win_amount),
                'win_number' => json_encode($order->win_number)
            ];
        }
        
        return $this->sendResponse(['ticket' => $ticket, 'total_amount' => currencyFormat($orders->sum('amount'))." R",
                                    'date' => date('Y-m-d H:i A',strtotime($orders[0]->created_at)),
                                    'result_time' => date('Y-m-d H:i A',$orders[0]->cycle->result_time),
                                    'username' =>   $orders[0]->user->username, 'account_number' => $orders[0]->user->account_number,
                                    'total_win_amount' => currencyFormat($orders->sum('win_amount'))." R", 'orders' => $ordersFormat,
                                    ], 'succesfully.', 200);
    }

    private function checkSufficientBalance($user,$orders,$promotion,$game){
        if(!$user->dCashAccount){
            UserAccount5D::createCashAccount($user->id);
            return $this->sendError(api_trans('account.insufficient_balance'));
        }
        $numberOfOrders = $promotion ? count($orders) - $this->getNumberOfBonusOrders(count($orders),$promotion) : count($orders);
        $totalAmount = $numberOfOrders * $game->order_price;
        if($user->dBonusAccount){
            if($user->dBonusAccount->balance < $totalAmount){
                if($user->dBonusAccount->balance + $user->dCashAccount->balance < $totalAmount){
                    return false;
                }
            }
        }
        else{
            if($user->dCashAccount->balance < $totalAmount){
                return false;
            }
        }
        return true;
    }

    private function checkValidBetNumber($orders = [], $game){
        foreach($orders as $order){ 
            for($i=0; $i<6; $i++){ 
                if(!in_array($order[$i], range($game->min, $game->max))){
                    return  false;
                }
            }
        }
        return true;
    }

    private function getGameRebate($user,$game){
        $rebate = null;
        if($user->is_shop){
            $rebate = GameRebate::whereGameTypeId($game->id)->whereRootId(2444)->first();
        }else{
            if($user->level != 1){
                $rebate = GameRebate::whereGameTypeId($game->id)->whereRootId($user->l1_id)->first();
            }else{
                $rebate = GameRebate::whereGameTypeId($game->id)->whereRootId($user->id)->first();
            }
        }
        if(!$rebate) $rebate = GameRebate::whereGameTypeId($game->id)->first();

        return $rebate;
    }

    private function generateTicketNumber($cycle_sn,$user_id){
        $numberOfOrders = addPrefixStringPad(OrderTemp::whereUserId($user_id)->get()->groupBy('ticket')->count() + 1,5  ,'0');
        $userId = addPrefixStringPad($user_id,4,'0');
        return $cycle_sn.$userId.$numberOfOrders; 
    }

    private function getNumberOfBonusOrders($numberOfOrders,$promotion){
        $numberOfBonusOrders = (int) ($numberOfOrders / ($promotion->target_order + $promotion->bonus_order))  * $promotion->bonus_order;
        return $numberOfBonusOrders;
    }
}
