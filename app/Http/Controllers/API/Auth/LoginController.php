<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;

class LoginController extends BaseController
{
    public function login(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);
            
        if($validator->fails()){
            return $this->sendError(api_trans('auth.field_required'));      
        }
        
        $user = User::where(['username' => $request->username])->orWhere('account_number',$request->username)->first();
        if(!$user || $user->password != md5($request->password.$user->pass_salt) 
            || $user->state !=1){ 
            return  $this->sendError(api_trans('auth.failed'));
        }

        $accessToken = $user->createToken('Ball')->accessToken;

        return $this->sendResponse(['username' => $user->username, 'id' => $user->id, 'accessToken' => $accessToken], api_trans('auth.successful_login'));
    }

    public function logout(){
        auth()->user()->token()->revoke();
        // $tokens = auth()->user()->tokens;
        // foreach($tokens as $token) {
        //     $token->revoke();   
        // }
        return $this->sendResponse('',api_trans('auth.successful_logout'));
    }
}
