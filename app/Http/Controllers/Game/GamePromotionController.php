<?php

namespace App\Http\Controllers\Game;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Promotion;
use DB;

class GamePromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotion = Promotion::first();
        return view('game.promotion.index',compact('promotion'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'target_order' => 'required|numeric',
            'bonus_order' => 'required|numeric'
        ]);
        $promotion = $this->saveToDB($request->all());
        return redirect()->route('games.promotion.index',compact('promotion'))->withSuccess('You have just added the game promotion successfully!');
    }

    public function update(Request $request, $id)
    {   
        $this->validate($request,[
            'target_order' => 'required|numeric',
            'bonus_order' => 'required|numeric'
        ]);
        $promotion = $this->saveToDB($request->all(),$id);
        return redirect()->route('games.promotion.index')->withSuccess('You have just updated the game promotion successfully!');
    }

    public function saveToDB($data,$id = null){ 
        DB::beginTransaction();
        try{
            $promotion = isset($id) ? Promotion::find($id) : new Promotion;
            if(!$promotion) return redirect()->back()->withError('There is no record found!');
            $data['from_date'] = strtotime($data['from_date']);
            $data['to_date'] = strtotime($data['to_date']);
            $promotion->fill($data);
            $promotion->save();
            
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $promotion;
    }
}
