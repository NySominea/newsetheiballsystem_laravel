<?php

namespace App\Http\Controllers\Game;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\GameType;
use DB;

class GameTypeController extends Controller
{
    public function index()
    {
        $games = GameType::all();
        return view('game.type.index',compact('games'));
    }


    public function store(Request $request)
    {
        $this->saveToDB($request->all());
        return redirect()->route('ball.games.types.index')->withSuccess('You have just updated game types successfully!');
    }


    public function saveToDB($data){
        foreach($data['games'] as $id => $row){
            $game = GameType::find($id);
            if($game){
                DB::beginTransaction();
                try{
                    $game->fill($row);
                    $game->save();
                    DB::commit();
                }catch(Exception $ex){
                    DB::rollback();
                }
            }
        }
    }
}
