<?php

namespace App\Http\Controllers\SystemSet\Location;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Commune;
use App\Model\District;
use App\Model\Province;
use DB;

class CommuneController extends Controller
{
    public function index()
    {   
        $communes = Commune::with('district','district.province')
                            ->when(request()->district_id && request()->district_id != 0,function($q){
                                return $q->whereDistrictId(request()->district_id);
                            })
                            ->orderBy('district_id','ASC')
                            ->orderBy('name','ASC')->paginate(40);
        $districts = ['0' => 'All'] + District::orderBy('name','ASC')->pluck('name','id')->toArray();
        return view('system-set.location.commune.index',compact('communes','districts'));
    }

    public function create()
    {
        $provinces = ['0' => 'Choose'] + Province::orderBy('name','ASC')->pluck('name','id')->toArray();
        $districts = request()->province_id ? ['0' => 'Choose'] + District::orderBy('name','ASC')->pluck('name','id')->toArray() : ['0' => 'Choose'];
        return view('system-set.location.commune.add-update',compact('provinces','districts'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:communes,name'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('system-set.locations.communes.index')->withSuccess('You have just added a commune successfully!');    
    }

    public function edit($id)
    {
        $commune = Commune::with('district','district.province')->whereId($id)->first();
        $provinces = ['0' => 'Choose'] + Province::orderBy('name','ASC')->pluck('name','id')->toArray();
        $districts = $commune->district && $commune->district->province 
                        ? District::orderBy('name','ASC')
                                ->whereProvinceId($commune->district->province->id)
                                ->pluck('name','id')->toArray()
                        : ['0' => 'Choose'];
        return view('system-set.location.commune.add-update',compact('commune','provinces','districts'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|unique:communes,name,'.$id
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('system-set.locations.communes.index')->withSuccess('You have just updated a commune successfully!');  
    }

    public function destroy($id)
    {
        $result = false;
        $item = Commune::findOrFail($id);
        if($item){
            DB::beginTransaction();
            try{
                if($item->delete()){
                    $result = true;
                }
                DB::commit();
            }catch(Exception $exception){
                DB::rollback();
                $result = false;
            }
        }
        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        $commune = isset($id) ? Commune::find($id) : new commune;
        DB::beginTransaction();
        try{
            if(!$commune) return redirect()->back()->withError('There is no record found!');
            $commune->fill($data);
            $commune->save();
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $commune;
    }
}
