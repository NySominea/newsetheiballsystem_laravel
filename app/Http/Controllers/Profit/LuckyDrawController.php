<?php

namespace App\Http\Controllers\Profit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cycle;
use Carbon\Carbon;

class LuckyDrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $years = Cycle::all()->groupBy(function($val) {
                            return Carbon::parse($val->created_at)->format('Y');
                        });
        foreach($years as $year => $row){
            $months = $years[$year]->groupBy(function($val) {
                                return Carbon::parse($val->created_at)->format('F');
                            });
            foreach($months as $month => $row){
                $data[$year][$month] = $row->sum('lucky_draw');
            }
        }
        
        return view('profit.lucky-draw.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
