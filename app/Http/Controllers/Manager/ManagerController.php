<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Manager;
use Cache;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managers = Manager::with('roles')->orderBy('id','DESC')->paginate(20);
        
        return view('admin.manager.index',compact('managers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all()->pluck('name','id');
        return view('admin.manager.add-update',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'username' => 'required|unique:sethei_mysql.tf_manager,username',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',
            'pay_pass' => 'required|confirmed|min:6',
            'pay_pass_confirmation' => 'required'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('ball.managers.index')->withSuccess('You have just added a manager successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all()->pluck('name','id');
        $manager = Manager::findOrFail($id);
        return view('admin.manager.add-update',compact('roles','manager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'username' => 'required|unique:sethei_mysql.tf_manager,username,'.$id,
        ]);
        
        if($request->password){
            $this->validate($request,[
                'password' => 'required|confirmed|min:6',
                'password_confirmation' => 'required',
            ]);
        }

        if($request->pay_pass){
            $this->validate($request,[
                'pay_pass' => 'required|confirmed|min:6',
                'pay_pass_confirmation' => 'required'
            ]);
        }

        $this->saveToDB($request->all(),$id);
        return redirect()->route('ball.managers.index')->withSuccess('You have just updated a manager successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $manager = isset($id) ? Manager::find($id) : new Manager;
            if(!$manager) return redirect()->back()->withError('There is no record found!');

            $data['PassSalt'] = isset($id) ? $manager->PassSalt : rand(10000000,99999999);
            if($data['password']){
                $data['Password'] = md5($data['password'].$data['PassSalt']);
            }
            if($data['pay_pass']){
                $data['PayPass'] = md5($data['pay_pass'].$data['PassSalt']);
            }
            $data['NickName'] = $data['NickName'] ?? '';
            $data['EmployeeID'] = '';
            $data['LastDT'] = date('Y-m-d H:i:s');
            $data['EditPw'] = date('Y-m-d');
            $manager->fill($data);
            $manager->save();

            $role = Role::find($data['role_id']);
            if($role) $manager->syncRoles([$role->name]);
            
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $manager;
    }
}
