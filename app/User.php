<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use App\Model\UserAccount;
use App\Constants\Account;
use App\Constants\UserBallAccount;
use App\Constants\User5dAccount;
use App\Model\UserAccount5D;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $fillable = [
        'id',
        'username',
        'user_type',
        'com_direct',
        'account_number',
        'email',
        'phone',
        'password',
        'pay_pass',
        'pass_salt',
        'real_name',
        'win_money_5d',
        'commission_5d',
        'current_commission_5d',
        'profit_5d',
        'win_money_ball',
        'commission_ball',
        'current_commission_ball',
        'profit_ball',
        'is_participate',
        'level',
        'parent_id',
        'l1_id',
        'l2_id',
        'l3_id',
        'l4_id',
        'l5_id',
        'l6_id',
        'l7_id',
        'l8_id',
        'state',
        'lang_id',
        'last_edit_password',
        'last_login',
        'address',
        'contact_number',
        'province_id',
        'district_id',
        'commune_id',
        'sale_supervisor',
        'sale_area_manager',
        'sale_director',
        'is_shop',
        'is_test',
        'can_bet'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public $timestamps = false;

    public function ballAccounts(){
        return $this->hasMany(UserAccount::class,'user_id','id');
    }

    public function ballCashAccount(){
        return $this->hasOne(UserAccount::class,'user_id','id')->where('type_id', UserBallAccount::CASH_ACCOUNT);
    }
    public function ballProfitAccount(){
        return $this->hasOne(UserAccount::class,'user_id','id')->where('type_id', UserBallAccount::PROFIT_ACCOUNT);
    }
    public function ballBonusAccount(){
        return $this->hasOne(UserAccount::class,'user_id','id')->where('type_id', UserBallAccount::BONUS_ACCOUNT);
    }

    public function dUserAccounts(){
        return $this->hasMany(UserAccount5D::class,'user_id','id');
    }

    public function dCashAccount(){
        return $this->hasOne(UserAccount5D::class,'user_id','id')->where('type_id', User5dAccount::CASH_ACCOUNT);
    }
    public function dProfitAccount(){
        return $this->hasOne(UserAccount5D::class,'user_id','id')->where('type_id', User5dAccount::PROFIT_ACCOUNT);
    }
    public function dBonusAccount(){
        return $this->hasOne(UserAccount5D::class,'user_id','id')->where('type_id', User5dAccount::BONUS_ACCOUNT);
    }

    public function children(){
        return $this->hasMany(User::class,'parent_id','id');
    }

    public function sale(){
        return $this->belongsTo(User::class,'parent_id','id');
    }

    public function parent(){
        return $this->belongsTo(User::class,'parent_id','id');
    }

    public function district(){
        if($this->Level <= 3) return $this;
        return $this->parent->district();
    }

    public function province(){
        return $this->district()->parent;
    }    
    public function main_balance(){
        return $this->hasOne(UserAccount::class)->whereTypeId(1);
    }

    public function profit_account(){
        return $this->hasOne(UserAccount::class)->whereTypeId(4);
    }

    public function default_account(){
        return $this->hasOne(UserAccount::class)->whereTypeId(1)->first();
    }

    public function bonus_account(){
        return $this->hasOne(UserAccount::class)->whereTypeId(5)->first();
    }

    public static function checkPayPassword($userId,$pwd){
        $user = User::find($userId);
        if(!$user || $user->pay_pass != md5($pwd.$user->pass_salt) 
            || $user->state !=1){
            return false;
		}
        return true;
    }

}
