<?php

Route::get('/',function(){ return redirect()->route('login'); });

Route::group(['namespace' => 'Auth'], function(){
    
    Route::get('login', ['as' => 'login', 'uses' => 'LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'LoginController@login']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
});

Route::group(['middleware' => 'auth'],function(){
    Route::get('/language/set-locale/{locale}',['uses' => 'SystemSet\Language\LanguageController@setLocale', 'as' => 'language.set-locale']);

    Route::get('/dashboard',['uses' => 'Dashboard\DashboardController@index','as' => 'dashboard']);

    Route::group(['namespace' => 'User'],function(){
        Route::group(['middleware' => ['permission:user-take-cash']], function () {
            Route::resource('/users/account/takecash', 'AccountTakeCashController',['as' => 'users.account']);
        });
        Route::group(['middleware' => ['permission:user-recharge']], function () {
            Route::resource('/users/account/recharge', 'AccountRechargeController',['as' => 'users.account']);
        });
        Route::group(['middleware' => ['permission:view-user-account-log']], function () {
            Route::resource('/users/account/logs', 'AccountLogController',['as' => 'users']);
        });
        Route::group(['middleware' => ['permission:view-user-account']], function () {
            Route::resource('/users/accounts', 'AccountController',['as' => 'users']);
        });
        Route::group(['middleware' => ['permission:view-user-account|view-user-account-log|user-recharge|user-take-cash']], function () {
            Route::resource('/users/sale-network', 'SaleNetworkController',['as' => 'users']);
            Route::resource('/users/direct-user', 'DirectUserController',['as' => 'users']);
        });
    });

    Route::group(['namespace' => 'Order'],function(){
        Route::group(['middleware' => ['permission:export-order-list']], function () {
            Route::get('/orders/temp/download',['uses' => 'OrderTempController@downloadExcel', 'as' => 'orders.temp.download']);
        });
        Route::group(['middleware' => ['permission:export-history-report']], function () {
            Route::get('/orders/report/download',['uses' => 'OrderController@downloadExcel', 'as' => 'orders.report.download']);
        });

        Route::group(['middleware' => ['permission:view-order-list']], function () {
            Route::get('/orders/temp/{ticket}/get-order-detail',['uses' => 'OrderTempController@ajaxGetOrderDetail', 'as' => 'orders.temp.get-order-detail']);
            Route::resource('/orders/temp', 'OrderTempController',['as' => 'orders']);
        });

        Route::group(['middleware' => ['permission:view-history-report']], function () {
            Route::get('/orders/report/{ticket}/get-order-detail',['uses' => 'OrderController@ajaxGetOrderDetail', 'as' => 'orders.report.get-order-detail']);
            Route::get('/orders/report/selection-form',['uses' => 'OrderController@showSelectionForm','as' => 'orders.report.selection-form']);
            Route::resource('/orders/report', 'OrderController',['as' => 'orders']);
        });
    });

    Route::group(['namespace' => 'Finance'],function(){
        Route::group(['middleware' => ['permission:download-transaction-log']], function () {
            Route::get('/finance/accounts/logs/download', ['uses' => 'AccountLogController@download','as' => 'accounts.logs.download']);
        });
        Route::group(['middleware' => ['permission:view-transaction-log']], function () {
            Route::resource('/finance/accounts/logs', 'AccountLogController',['as' => 'accounts']);    
        });
        Route::group(['middleware' => ['permission:platform-account-access-transfer']], function () {
            Route::resource('/finance/accounts/transfer', 'AccountTransferController',['as' => 'accounts']);
        });
        Route::group(['middleware' => ['permission:platform-account-access-transfer']], function () {
            Route::resource('/finance/accounts/deposit', 'AccountDepositController',['as' => 'accounts']);
        });   
        Route::group(['middleware' => ['permission:add-class-manager']], function () {
            Route::resource('/finance/accounts/categories', 'AccountCategoryController',['as' => 'accounts']);
        });   
        Route::group(['middleware' => ['permission:view-platform-account|add-new-account']], function () {
            Route::resource('/finance/accounts', 'AccountController'); 
        });
    });

    Route::group(['namespace' => 'Cycle'],function(){
        Route::group(['middleware' => ['permission:view-lottery-log']], function () {
            Route::resource('/cycles/logs', 'CycleLogController',['as' => 'cycles']);
        });
        Route::group(['middleware' => ['permission:lottery-setting']], function () {
            Route::resource('/cycles/set', 'CycleSetController',['as' => 'cycles']);
        });
        Route::group(['middleware' => ['permission:lottery-operation']], function () {
            Route::get('cycles/result/set',['uses' => 'ResultSettleController@setResultForm','as' => 'cycles.result.set']);
            Route::post('cycles/result/set',['uses'  => 'ResultSettleController@setResult','as' => 'cycles.result.set']);
        });
        Route::group(['middleware' => ['permission:billing-operation']], function () {
            Route::get('cycles/result/settle',['uses' => 'ResultSettleController@settleResultLoading','as' => 'cycles.result.settle']);
            Route::post('cycles/result/settle',['uses' => 'ResultSettleController@settleResult','as' => 'cycles.result.settle']);
            Route::post('ajax/cycle/result/settle',['uses' => 'ResultSettleController@ajaxSettleResult','as' => 'ajax.cycles.result.settle']);
        });
    });

    Route::group(['namespace' => 'Profit'],function(){
        Route::group(['middleware' => ['permission:lucky-draw']], function () {
            Route::resource('/profit/lucky-draw', 'LuckyDrawController',['as' => 'profit']);
        });
    });

    Route::group(['namespace' => 'Manager'],function(){
        Route::group(['middleware' => ['permission:add-new-role|view-role|role-modification']], function () {
            Route::resource('/managers/roles', 'ManagerRoleController',['as' => 'managers']);
        });
        Route::group(['middleware' => ['permission:view-manager-log']], function () {
            Route::resource('/managers/logs', 'ManagerLogController',['as' => 'managers']);
        });

        Route::resource('/managers/password','ManagerEditPasswordController',['as' => 'managers']);
        Route::group(['middleware' => ['permission:add-new-manager-account|view-manager-account|manager-account-modification']], function () {
            Route::resource('/managers', 'ManagerController');
        });
    });

    Route::group(['namespace' => 'Game'],function(){
        Route::group(['middleware' => ['permission:view-game-type-setting|game-type-modification']], function () {
            Route::resource('/games/types', 'GameTypeController',['as' => 'games']);
        });
        Route::group(['middleware' => ['permission:view-game-rebate-setting|game-rebate-modification']], function () {
            Route::resource('/games/rebates', 'GameRebateController',['as' => 'games']);
        });
        Route::group(['middleware' => ['permission:view-game-promotion|game-promotion-modification']], function () {
            Route::resource('/games/promotion', 'GamePromotionController',['as' => 'games']);
        });
    });    
    
    Route::group(['namespace' => 'SystemSet','as' => 'system-set.'],function(){
        Route::group(['middleware' => ['permission:view-exchange-rate-set|exchange-rate-modification']], function () {
            Route::resource('/system-set/exchange-rate', 'ExchangeRate\ExchangeRateController');
        });
        Route::group(['middleware' => ['permission:view-language-set|language-set-modification']], function () {
            Route::resource('/system-set/languages/admin', 'Language\LanguageAdminController',['as' => 'languages']);
            Route::resource('/system-set/languages/api', 'Language\LanguageApiController',['as' => 'languages']);
        });
        Route::group(['middleware' => ['permission:view-location-set|location-set-modification']], function () {
            Route::resource('/system-set/locations/provinces', 'Location\ProvinceController',['as' => 'locations']);
            Route::resource('/system-set/locations/districts', 'Location\DistrictController',['as' => 'locations']);
            Route::resource('/system-set/locations/communes', 'Location\CommuneController',['as' => 'locations']);
        });
    });

    Route::group(['namespace' => 'Analysis'],function(){
        Route::group(['middleware' => ['permission:analysis-operation']], function () {
            Route::resource('/analysis', 'ResultAnalysisController');
        });
    });

    // Ajax Routes
    Route::get('get-district-by-province',['uses' => 'SystemSet\Location\DistrictController@getDistrictsByProvinceId', 'as' => 'locations.get-districts-by-province']);
    Route::get('get-system-account-log-info/{id}',['uses' => 'Finance\AccountLogController@ajaxGetLogInformation', 'as' => 'system.account.getLogInfo']);
    Route::get('get-user-account-log-info/{id}',['uses' => 'User\AccountLogController@ajaxGetLogInformation', 'as' => 'user.account.getLogInfo']);
    
    
}); 